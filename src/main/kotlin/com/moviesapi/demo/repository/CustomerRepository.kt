package com.moviesapi.demo.repository

import com.moviesapi.demo.entity.Customer
import org.springframework.data.repository.CrudRepository

interface CustomerRepository:CrudRepository<Customer,Long>
