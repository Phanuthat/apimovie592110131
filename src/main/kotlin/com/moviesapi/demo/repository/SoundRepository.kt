package com.moviesapi.demo.repository

import com.moviesapi.demo.entity.Sound
import org.springframework.data.repository.CrudRepository

interface SoundRepository :CrudRepository<Sound,Long> {

}