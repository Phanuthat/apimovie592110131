package com.moviesapi.demo.repository

import com.moviesapi.demo.entity.Movie
import org.springframework.data.repository.CrudRepository

interface MovieRepository:CrudRepository<Movie,Long>
