package com.moviesapi.demo.repository

import com.moviesapi.demo.entity.CycleTime
import org.springframework.data.repository.CrudRepository

interface CycleTimeRepository:CrudRepository<CycleTime,Long> {
    fun findByMovie_NameContainingIgnoreCase(name: String): List<CycleTime>
}