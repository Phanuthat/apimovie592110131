package com.moviesapi.demo.repository

import com.moviesapi.demo.entity.Seat
import org.springframework.data.repository.CrudRepository

interface SeatRepository:CrudRepository<Seat,Long> {
}