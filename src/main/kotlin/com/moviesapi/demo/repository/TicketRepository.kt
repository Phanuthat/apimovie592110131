package com.moviesapi.demo.repository

import com.moviesapi.demo.entity.Ticket
import org.springframework.data.repository.CrudRepository


interface TicketRepository : CrudRepository<Ticket,Long>{

     fun findByCycleTime_Movie_NameContainingIgnoreCase(name: String): List<Ticket>
     fun findByCustomer_EmailContainingIgnoreCaseOrCustomer_FirstName(email:String,firstName:String):List<Ticket>
}