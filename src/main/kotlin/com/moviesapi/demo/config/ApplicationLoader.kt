package com.moviesapi.demo.config

import com.moviesapi.demo.entity.*
import com.moviesapi.demo.repository.*
import com.moviesapi.demo.security.entity.Authority
import com.moviesapi.demo.security.entity.AuthorityName
import com.moviesapi.demo.security.entity.JwtUser
import com.moviesapi.demo.security.repository.AuthorityRepository
import com.moviesapi.demo.security.repository.UserRepository

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component
import javax.transaction.Transactional

@Component
class ApplicationLoader : ApplicationRunner {


    @Autowired
    lateinit var customerRepository: CustomerRepository

    @Autowired
    lateinit var movieRepository: MovieRepository

    @Autowired
    lateinit var cinemaRepository: CinemaRepository

    @Autowired
    lateinit var subtitleRepository: SubtitleRepository

    @Autowired
    lateinit var soundRepository: SoundRepository

    @Autowired
    lateinit var cycleTimeRepository: CycleTimeRepository

    @Autowired
    lateinit var seatRepository: SeatRepository


    @Autowired
    lateinit var ticketRepository: TicketRepository

    @Autowired
    lateinit var dataLoader: DataLoader

    @Autowired
    lateinit var authorityRepository: AuthorityRepository
    @Autowired
    lateinit var userRepository: UserRepository
    @Transactional
    fun loadUsernameAndPassword() {
        val auth1 = Authority(name = AuthorityName.ROLE_ADMIN)
        val auth2 = Authority(name = AuthorityName.ROLE_CUSTOMER)
        val auth3 = Authority(name = AuthorityName.ROLE_GENERAL)
        authorityRepository.save(auth1)
        authorityRepository.save(auth2)
        authorityRepository.save(auth3)
        val encoder = BCryptPasswordEncoder()
        val custl = Customer(firstName = "อมชาติ", email = "a@b.com")
        val custJwt = JwtUser(
                username = "customer",
                password = encoder.encode("password"),
                email = custl.email,

                enabled = true,
                firstname = custl.firstName,
                lastname = "unknown"
        )

        customerRepository.save(custl)
        userRepository.save(custJwt)
        custl.jwtUser = custJwt
        custJwt.user=custl
        custJwt.authorities.add(auth1)
        custJwt.authorities.add(auth2)
        custJwt.authorities.add(auth3)
    }

    @Transactional
    override fun run(args: ApplicationArguments?) {


        customerRepository.save(Customer("test", "test", UserStatus.ACTIVE, "test@gmail.com", "1234"))
        customerRepository.save(Customer("test12", "test12", UserStatus.ACTIVE, "test12@gmail.com", "1234"))
        customerRepository.save(Customer("test123", "test123", UserStatus.ACTIVE, "test123@gmail.com", "1234"))


        var subtitleTh = subtitleRepository.save(Subtitle("th"))
        var subtitleEn = subtitleRepository.save(Subtitle("en"))
        var subtitleJp = subtitleRepository.save(Subtitle("jp"))
        var subtitle = subtitleRepository.save(Subtitle("no sub"))

        var soundTh = soundRepository.save(Sound("th"))
        var soundEn = soundRepository.save(Sound("en"))
        var soundEn1 = soundRepository.save(Sound("en"))
        val movie = movieRepository.save(Movie("007 - 23 Skyfall", "Bond's loyalty to M is tested when her past comes back to haunt her. When MI6 comes under attack, 007 must track down and destroy the threat, no matter how personal the cost.", "James Bond", "104 min", "https://www.imdb.com/title/tt1074638/mediaviewer/rm2415138816"))
        movie.subtitle.add(subtitleTh)
        movie.sound.add(soundEn)

        val movie1 = movieRepository.save(Movie("Big Fish", "A frustrated son tries to determine the fact from fiction in his dying father's life.", "Fantasy", "110 min", "https://www.imdb.com/title/tt0319061/mediaviewer/rm2614205696"))
        movie1.sound.add(soundTh)
        movie1.subtitle.add(subtitleEn)
        movie1.subtitle.add(subtitle)

        var seat = seatRepository.save(Seat(SeatType.SOFA, 1, 4))
        var seatPremiums = seatRepository.save(Seat(SeatType.PREMIUMN, 5, 5))
        var SearDeluxe = seatRepository.save(Seat(SeatType.DELUXE, 5, 5))


        var seatPremiums1 = seatRepository.save(Seat(SeatType.PREMIUMN, 5, 5))
        var SearDeluxe1 = seatRepository.save(Seat(SeatType.DELUXE, 5, 5))

        val cinema = cinemaRepository.save(Cinema("Cinema 1"))
        cinema.Seat.add(seat)
        cinema.Seat.add(seatPremiums)
        cinema.Seat.add(SearDeluxe)

        val cinema1 = cinemaRepository.save(Cinema("Cinema 2"))
        cinema1.Seat.add(seat)
        cinema1.Seat.add(seatPremiums)
        cinema1.Seat.add(SearDeluxe)

        val cinema3 = cinemaRepository.save(Cinema("Cinema 3"))

        cinema3.Seat.add(seatPremiums1)
        cinema3.Seat.add(SearDeluxe1)

        var cycleTime = cycleTimeRepository.save(CycleTime(1553414400,1553421600))
        cycleTime.cinema =cinema1
        cycleTime.movie.add(movie1)
        cycleTime.sound =soundTh
        cycleTime.subtitle=subtitleEn

        var cycleTime1 = cycleTimeRepository.save(CycleTime(1553490000,1553496000))
        cycleTime1.cinema =cinema3
        cycleTime1.movie.add(movie)
        cycleTime1.sound =soundEn1
        cycleTime1.subtitle=subtitleJp

        dataLoader.loadData()


       var ticket =  ticketRepository.save(Ticket("AA",false,0,0,250))
        var ticket1 =ticketRepository.save(Ticket("AA",false,0,1,250))
       var ticket2 = ticketRepository.save(Ticket("AA",false,0,2,250))
        var ticket3 = ticketRepository.save(Ticket("AA",false,0,3,250))
        var ticket4 = ticketRepository.save(Ticket("AA",false,0,4,250))
        var ticket5 = ticketRepository.save(Ticket("AA",false,0,5,250))
        var ticket6 = ticketRepository.save(Ticket("AA",false,0,6,250))

        ticket.cycleTime = cycleTime
        ticket1.cycleTime = cycleTime
        ticket2.cycleTime = cycleTime
        ticket3.cycleTime = cycleTime
        ticket4.cycleTime = cycleTime
        ticket5.cycleTime = cycleTime
        ticket6.cycleTime = cycleTime
        loadUsernameAndPassword()







    }

}