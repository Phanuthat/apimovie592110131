package com.moviesapi.demo.dao

import com.moviesapi.demo.entity.Seat
import com.moviesapi.demo.repository.SeatRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class SeatDaoDBImpl :SeatDao{
    @Autowired
    lateinit var seatRepository: SeatRepository
    override fun getSeat(): List<Seat> {
        return seatRepository.findAll().filterIsInstance(Seat::class.java)
    }
}