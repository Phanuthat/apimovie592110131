package com.moviesapi.demo.dao


import com.moviesapi.demo.entity.Dto.SoundDto
import com.moviesapi.demo.entity.Sound

interface SoundDao {
    fun getAllSound(): List<Sound>
    fun save(sound: Sound): Sound


}