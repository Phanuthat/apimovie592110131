package com.moviesapi.demo.dao


import com.moviesapi.demo.entity.Sound
import com.moviesapi.demo.repository.SoundRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository


@Profile("db")
@Repository
class SoundDaoDBImpl:SoundDao {
    override fun save(sound: Sound): Sound {
        return soundRepository.save(sound)
    }


    @Autowired
    lateinit var soundRepository: SoundRepository
    override fun getAllSound(): List<Sound> {
        return soundRepository.findAll().filterIsInstance(Sound::class.java)
    }
}