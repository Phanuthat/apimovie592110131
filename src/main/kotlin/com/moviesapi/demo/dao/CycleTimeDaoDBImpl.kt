package com.moviesapi.demo.dao

import com.moviesapi.demo.entity.CycleTime
import com.moviesapi.demo.repository.CycleTimeRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import java.util.*


@Profile("db")
@Repository
class CycleTimeDaoDBImpl :CycleTimeDao{
    override fun findById(id: Long): CycleTime {
        return cycleTimeRepository.findById(id).orElse(null)
    }

    override fun save(it: CycleTime): CycleTime {
        return cycleTimeRepository.save(it)
    }

    override fun getCycleTimeByMovie(name: String): List<CycleTime> {
        return cycleTimeRepository.findByMovie_NameContainingIgnoreCase(name)
    }

    override fun getAllCycleTime(): List<CycleTime> {
        return cycleTimeRepository.findAll().filterIsInstance(CycleTime::class.java)
    }

    @Autowired
    lateinit var cycleTimeRepository:CycleTimeRepository


}