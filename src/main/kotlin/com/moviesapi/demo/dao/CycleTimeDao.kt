package com.moviesapi.demo.dao

import com.moviesapi.demo.entity.CycleTime

interface CycleTimeDao {
    fun getAllCycleTime(): List<CycleTime>
    fun getCycleTimeByMovie(name: String): List<CycleTime>
    fun save(it: CycleTime): CycleTime
    fun findById(id: Long): CycleTime
}