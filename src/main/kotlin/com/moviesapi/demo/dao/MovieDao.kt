package com.moviesapi.demo.dao

import com.moviesapi.demo.entity.Movie

interface MovieDao {
    fun getMovies() : List<Movie>
}