package com.moviesapi.demo.dao

import com.moviesapi.demo.entity.Customer

interface CustomerDao {
    fun getCustomer():List<Customer>
    fun findById(id: Long): Customer
    fun save(mapCustomer: Customer): Customer

}