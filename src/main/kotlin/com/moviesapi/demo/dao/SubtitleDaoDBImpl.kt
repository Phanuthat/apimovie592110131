package com.moviesapi.demo.dao



import com.moviesapi.demo.entity.Subtitle
import com.moviesapi.demo.repository.SubtitleRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class SubtitleDaoDBImpl:SubtitleDao {
    override fun save(subtitle: Subtitle): Subtitle {
        return subtitleRepository.save(subtitle)
    }

    @Autowired
    lateinit var subtitleRepository: SubtitleRepository
    override fun getAllSubtitle(): List<Subtitle> {
        return subtitleRepository.findAll().filterIsInstance(Subtitle::class.java)
    }
}