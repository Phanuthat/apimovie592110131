package com.moviesapi.demo.dao

import com.moviesapi.demo.entity.Subtitle

interface SubtitleDao{
    fun getAllSubtitle():List<Subtitle>
    fun save(subtitle: Subtitle): Subtitle
}