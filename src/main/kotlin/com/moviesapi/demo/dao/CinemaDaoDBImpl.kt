package com.moviesapi.demo.dao

import com.moviesapi.demo.entity.Cinema
import com.moviesapi.demo.repository.CinemaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class CinemaDaoDBImpl:CinemaDao {


    @Autowired
    lateinit var cinemaRepository: CinemaRepository
    override fun getCinema(): List<Cinema> {
        return cinemaRepository.findAll().filterIsInstance(Cinema::class.java)
    }
}