package com.moviesapi.demo.controller

import com.moviesapi.demo.Util.MapperUtil
import com.moviesapi.demo.entity.CycleTime
import com.moviesapi.demo.service.CycleTimeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController


@RestController
class CycleTimeController {
    @Autowired
    lateinit var cycleTimeService: CycleTimeService
    @GetMapping("/cycletime")
    fun getAllCycleTime(): ResponseEntity<Any> {
        val cycleTimeAll = cycleTimeService.getAllCycleTime()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapCyCleTimeDto(cycleTimeAll))

    }

    @GetMapping("/cycletime/{movie}")
    fun getCycletimeByMovieName(@PathVariable("movie")name:String):ResponseEntity<Any>{
        val output =MapperUtil.INSTANCE.mapCyCleTimeDto(cycleTimeService.getCycletimeByMovieName(name))
        return ResponseEntity.ok(output)
    }

}