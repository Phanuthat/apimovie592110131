package com.moviesapi.demo.controller

import com.moviesapi.demo.Util.MapperUtil
import com.moviesapi.demo.entity.Dto.SubtitleDto
import com.moviesapi.demo.entity.Subtitle
import com.moviesapi.demo.service.SubtitleService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController


@RestController
class SubtitleController {

    @Autowired
    lateinit var subtitleService: SubtitleService


    @GetMapping("/subtitle")
    fun getAllSutitle(): ResponseEntity<Any> {
        val allSubtitle = subtitleService.getAllSubtitle()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapSubTitleDto(allSubtitle))

    }

    @PostMapping("/subtitle")
    fun add(@RequestBody sub: Subtitle): ResponseEntity<Any> {
        val output = subtitleService.save(sub)
        return ResponseEntity.ok(output)

    }
}