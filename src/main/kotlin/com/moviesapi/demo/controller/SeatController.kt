package com.moviesapi.demo.controller

import com.moviesapi.demo.Util.MapperUtil
import com.moviesapi.demo.service.SeatService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController


@RestController
class SeatController {
    @Autowired
    lateinit var seatService: SeatService
    @GetMapping("/seattype")
    fun getCustomer(): ResponseEntity<Any> {
        val seat = seatService.getSeatType()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapSeatDto(seat))
    }
}