package com.moviesapi.demo.controller

import com.moviesapi.demo.Util.MapperUtil

import com.moviesapi.demo.service.CinemaService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping

import org.springframework.web.bind.annotation.RestController


@RestController
class CinemaController {
    @Autowired
    lateinit var cinemaService: CinemaService

    @GetMapping("/cinema")
    fun getCustomer(): ResponseEntity<Any> {
        val cinemaAll = cinemaService.getCinema()

        return ResponseEntity.ok(MapperUtil.INSTANCE.mapCinemaDto(cinemaAll))
    }


}