package com.moviesapi.demo.controller

import com.moviesapi.demo.Util.MapperUtil
import com.moviesapi.demo.entity.Dto.SoundDto
import com.moviesapi.demo.entity.Sound
import com.moviesapi.demo.service.SoundService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController


@RestController
class SoundController {
    @Autowired
    lateinit var  soundService: SoundService

    @GetMapping("/sound")
    fun getAllSound(): ResponseEntity<Any> {

        val allSound = soundService.getAllSound()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapSoundDto(allSound))
    }

    @PostMapping("/sound")
    fun add(@RequestBody sound: SoundDto):ResponseEntity<Any>{
        val output = soundService.save(MapperUtil.INSTANCE.mapSound(sound))
        val outputDto =MapperUtil.INSTANCE.mapSoundDto(output)
        outputDto.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()

    }
}