package com.moviesapi.demo.security.repository


import com.moviesapi.demo.security.entity.Authority
import com.moviesapi.demo.security.entity.AuthorityName
import org.springframework.data.repository.CrudRepository

interface AuthorityRepository: CrudRepository<Authority, Long> {
    fun findByName(input: AuthorityName): Authority
}