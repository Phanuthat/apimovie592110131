package com.moviesapi.demo.entity.Dto

import com.moviesapi.demo.entity.Customer
import javax.persistence.Column

class TicketDto(
        var id: Long? = null,
        var seatName: String? = null,
        var seatStatus: Boolean = false,
        @Column(name = "seatRow")
        var row: Int? = null,
        @Column(name = "seatCol")
        var column: Int? = null,
        var cycleTime: CycleTimeDto? = null,
        var customer: CustomerDto? = null,
        var price : Int?=null) {
}