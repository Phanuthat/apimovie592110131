package com.moviesapi.demo.entity.Dto


import java.text.DateFormat
import java.util.*

class CycleTimeDto(var id:Long?=null,
                   var startTime: Long?=null,
                   var endTime : Long?=null,
                   var movie:List<MovieDtoForCycleTime>?=null,
                   var sound:SoundDto?=null,
                   var subtitle:SubtitleDto?=null,
                   var cinema:CinemaDto?=null
                   ) {
}