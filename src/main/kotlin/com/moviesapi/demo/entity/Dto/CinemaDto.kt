package com.moviesapi.demo.entity.Dto




data class CinemaDto(var name: String? = null,
                     var id: Long? = null,
                     var seat: List<SeatDto> = mutableListOf())

