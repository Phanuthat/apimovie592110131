package com.moviesapi.demo.entity.Dto

class MovieDto(var id: Long? = null,
               var name: String? = null,
               var description: String? = null,
               var genre: String? = null,
               var runtime: String? = null,
               var ImageUrl: String? = null,

               var subtitle: List<SubtitleDto>? = null,
               var sound:List<SoundDto>?=null) {
}