package com.moviesapi.demo.entity.Dto

import com.moviesapi.demo.entity.SeatType
import javax.persistence.Column

class SeatDto(var seatType : SeatType?=null,
              @Column(name="seatRow")
              var row :Int?=null,
              @Column(name="seatCol")
              var column:Int?=null,
              var id:Long?=null) {
}