package com.moviesapi.demo.entity.Dto

data class MovieDtoForCycleTime(var id: Long? = null,
                                var name: String? = null,
                                var description: String? = null,
                                var genre: String? = null,
                                var runtime: String? = null,
                                var ImageUrl: String? = null)