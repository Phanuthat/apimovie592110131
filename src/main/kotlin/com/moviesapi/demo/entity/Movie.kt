package com.moviesapi.demo.entity

import javax.persistence.*

@Entity
data class Movie(var name:String?=null,
                 var description: String?=null,
                 var genre:String?=null,
                 var runtime:String?=null,
                 var ImageUrl:String?=null){
    @Id
    @GeneratedValue
    var id:Long? =null

    @OneToMany
        var subtitle = mutableListOf<Subtitle>()

    constructor(name:String,description: String,genre:String,runtime:String,ImageUrl:String,subtitle: Subtitle):this(name,description,genre,runtime,ImageUrl){
        this.subtitle = subtitle as MutableList<Subtitle>
    }
    @OneToMany
        var sound = mutableListOf<Sound>()
}
