package com.moviesapi.demo.entity

import com.moviesapi.demo.security.entity.JwtUser
import javax.persistence.*

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
abstract class User(
        open var firstName: String? = null,
        open var lastName: String? = null,
        open var userStatus: UserStatus? = null,
        open var email: String? = null,
        open var password: String? = null,
        open var image: String? = null

) {
    @Id
    @GeneratedValue
    var id: Long? = null

    @OneToOne
    var jwtUser: JwtUser? = null
}





