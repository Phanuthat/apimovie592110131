package com.moviesapi.demo.entity


import java.util.*
import javax.persistence.*


@Entity
data class CycleTime(var startTime: Long? = null,
                     var endTime: Long? = null
                     ) {
    @Id
    @GeneratedValue
    var id: Long? = null

    @OneToMany
    var movie = mutableListOf<Movie>()


    @ManyToOne
    var sound: Sound? = null

    @OneToOne
    var subtitle: Subtitle? = null

    @ManyToOne
    var cinema: Cinema? = null

}
