package com.moviesapi.demo.service

import com.moviesapi.demo.dao.CustomerDao
import com.moviesapi.demo.entity.Customer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile

@Service
class CustomerServiceImpl : CustomerService {
    override fun saveImgUrl(id: Long, file: MultipartFile): Customer {
      var customer = customerDao.findById(id)
        var img = this.amazonClient.uploadFile(file)
        customer.image = img
        return customerDao.save(customer)

    }

    @Autowired
    lateinit var amazonClient: AmazonClient
    @Autowired
    lateinit var customerDao: CustomerDao




    override fun save(mapCustomer: Customer): Customer {
        return customerDao.save(mapCustomer)
    }


    override fun getCustomers(): List<Customer> {
        return customerDao.getCustomer()
    }

}