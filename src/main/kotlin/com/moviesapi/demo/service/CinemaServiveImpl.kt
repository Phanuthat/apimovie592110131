package com.moviesapi.demo.service

import com.moviesapi.demo.dao.CinemaDao

import com.moviesapi.demo.entity.Cinema
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CinemaServiveImpl : CinemaService {

    @Autowired
    lateinit var cinemaDao: CinemaDao

    override fun getCinema(): List<Cinema> {
        return cinemaDao.getCinema()
    }

}