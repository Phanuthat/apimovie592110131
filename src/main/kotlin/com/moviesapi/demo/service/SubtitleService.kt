package com.moviesapi.demo.service

import com.moviesapi.demo.entity.Dto.SubtitleDto
import com.moviesapi.demo.entity.Subtitle

interface SubtitleService {
    fun getAllSubtitle():List<Subtitle>
    fun save(sub: Subtitle): Subtitle
}