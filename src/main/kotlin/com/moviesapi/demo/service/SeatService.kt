package com.moviesapi.demo.service

import com.moviesapi.demo.entity.Seat

interface SeatService {
    fun getSeatType():List<Seat>
}